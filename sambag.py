#!/sdk/anaconda2/bin/python

import os
from configset import configset
import sys
import argparse

configname = os.path.join(os.path.dirname(__file__), 'sambag.ini')
CONFIG = configset(configname)

"""
Samba autogenerator samba path in file config.

This code is placed in the public domain by the author.
Written by Licface.

This is especially neat for Linux users, who (I think) don't
get any app for it in the default linux installation.

contact me at licface@yahoo.com for bugs info

Note that if you intend to send messages to mail, please send me the bugs too.
"""

__version__		= 0.1
__test__		= 0.1
__build__		= 2.7
__platform__	= "linux"
__author__		= "licface"
__email__ 		= "licface@yahoo.com"
__site__ 		= "licface@yahoo.com"

class sambagenerator(object):
	def __init__(self):
		super(sambagenerator, self)
		"""
			[apache2conf]
				path = /etc/apache2
				browsable = yes
				writable = yes
				read only = no
				guest only = no
				create mask = 0777
				directory mask = 0777
				guest ok = no
				force user = nobody
				public = yes
        """
		#configset = cfset()
		self.configname = os.path.join(os.path.dirname(__file__), 'sambag.ini')
		#CONFIG.configname = configname
		self.template = """[{0}]
path = {1}
browsable = {2}
writable = {3}
read only = {4}
guest only = {5}
create mask = {6}
directory mask = {7}
guest ok = {8}
force user = {9}
public = yes
{10}"""

	def getBool(self, data):
		if data == True:
			return "yes"
		elif data == False:
			return "no"

	def getKwargs(self, kwargs):
		data = ''
		for i in kwargs:
			data += str(kwargs.get(i)) + "\n"
		return data

	def generate(self, name=None, path=None, browsable=True, writable=True, read_only=False, guest_only=False, create_mask='0777', directory_mask='0777', guest_ok=False, verbose=None, username = "nobody", **kwargs):
		if name == None or path == None:
			return self.usage(True)
		data = [name, path, self.getBool(browsable), self.getBool(writable), self.getBool(read_only), self.getBool(guest_only), str(create_mask), str(directory_mask), self.getBool(guest_ok), username, self.getKwargs(kwargs)]
		if os.path.exists(CONFIG.get_config('CONFIG', 'PATH')):
			f = open(CONFIG.get_config('CONFIG', 'PATH'), 'a')
			f.write("\n")
			f.write(self.template.format(*data))
			f.close()
			if verbose:
				print "\n"
				print "\tcreate samba path ..."
				print "\n"
				print self.template.format(*data)
			return True
		return False

	def usage(self, print_help=None):
		parser = argparse.ArgumentParser()
		parser.add_argument('NAME', help='Name of Share Folder/Directory', action='store')
		parser.add_argument('PATH', help='Path of Share Folder/Directory', action='store')
		parser.add_argument('-b', '--browsable', help='Set Directory/Folder can indexes, default = True', action='store_false')
		parser.add_argument('-w', '--writable', help='Set Directory/Folder can write/add, default = True', action='store_false')
		parser.add_argument('-r', '--read-only', help='Set Directory/Folder read only, default = False', action='store_true')
		parser.add_argument('-g', '--guest-only', help='Set Directory/Folder can access by guest only, default = False', action='store_true')
		parser.add_argument('-m', '--mask', help='Set Directory/Folder mask, default = 0777', action='store', default='0777')
		parser.add_argument('-s', '--dir-mask', help='Set Directory/Folder throught subfolder mask, default = 0777', action='store', default='0777')
		parser.add_argument('-k', '--guest-ok', help='Set Directory/Folder can access by guest too, default = False', action='store_true')
		parser.add_argument('-l', '--others', help='Set other option, read manual samba for configuration', action='store', nargs='*')
		parser.add_argument('-n', '--username', help = 'Username credential', action = 'store')
		parser.add_argument('-v', '--verbose', help='Show process running', action='store_true')
		if len(sys.argv) == 1:
			parser.print_help()
		elif print_help:
			parser.print_help()
		else:
			args = parser.parse_args()
			# def generate(self, name=None, path=None, browsable=True, writable=True, read_only=False, guest_only=False, create_mask=0777, directory_mask=0777, guest_ok=False, verbose=None, **kwargs):
			if self.generate(args.NAME, args.PATH, args.browsable, args.writable, args.read_only, args.guest_only, args.mask, args.dir_mask, args.guest_ok, args.verbose, args.username) == True:
				pass
			else:
				parser.print_help()

if __name__ == '__main__':
	c = sambagenerator()
	c.usage()






